# -*- encoding: utf-8 -*-
"""
@File    : setup.py.py
@Time    : 2021/10/21 10:21 上午
@Author  : ZR
@Software: PyCharm
"""

from distutils.core import setup

setup(
    name="mytest",
    version="1.1",
    py_modules=["mytest1"],
    install_requires=['arrow>=0.10.0'],
)
